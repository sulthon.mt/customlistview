package com.example.customlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //a List of type hero for holding list items
    List<hero> heroList;
    //the listview
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing object
        heroList = new ArrayList<>();
        listView = findViewById(R.id.listView);

        //adding sosme values to our list
        heroList.add(new hero(R.drawable.spiderman, "Spiderman", "Avengers"));
        heroList.add(new hero(R.drawable.joker, "Joker", "Injustice gang"));
        heroList.add(new hero(R.drawable.ironman, "Iron Man", "Avengers"));
        heroList.add(new hero(R.drawable.doctorstrange, "Doctor Strange", "Avengers"));
        heroList.add(new hero(R.drawable.captainamerica, "Captain America", "Avengers"));
        heroList.add(new hero(R.drawable.batman, "Batman", "Justice League"));

        //creating the adapter
        MyListAdapter adapter = new MyListAdapter(this, R.layout.my_custom_list, heroList);

        //ataching adapter to the listview
        listView.setAdapter(adapter);


    }
}
